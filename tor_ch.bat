REM Call this file with admin rights CMD

shell = "powershell"

REM Installation von chocolatey
@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"

REM Installation of differet tools
choco install -y notepadplusplus
choco install -y python

REM alternatively: https://www.torproject.org/de/download/languages/
choco install -y tor-browser


REM C:\Users\myuser\Desktop\Tor Browser\Browser\TorBrowser\Data\Tor\torrc
REM add there  ExitNodes {ch} to fake ch ip s
python fileappandor.py

REM Test
start "C:\ProgramData\chocolatey\lib\tor-browser\tools\tor-browser\Browser\firefox.exe" https://whatismyipaddress.com/de/meine-ip

Pause



